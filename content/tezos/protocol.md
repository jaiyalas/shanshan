---
title: "protoco"
date: 2020-10-03
categories: []
hidden: false
katex: true
---

# prelude

## blockchain as global state monad

- From the *section 2.1 Mathematical representation* in the whitepaper
    - A blockchain protocol is fundamentally a monadic implementation of concurrent mutations of a global state
    - blocks are operators over a global state
    - a block \\(b\\) is defined by \\(b \in B \subset S^{S\cup\{\oslash\}}\\) where \\(B^A \equiv \{A \to B\}\\)
        - namely \\(\forall b \in B . b :: S\cup\{\oslash\} \to S\\)
- From *2.3 Functional representation* in the whitepaper
    - The state is represented with the help of a Context module which encapsulates a disk-based immutable key-value store.
        - implemented via the asynchronous monad Lwt
- From [The big abstraction barrier](https://tezos.gitlab.io/developer/entering_alpha.html#the-big-abstraction-barrier-alpha-context), **Alpha_context** defines
    - underlying connection to concrete storage structure (eventually, Irmin)
    - global state, `Alpha_context.t`, and related functions
    - [alpha_context.ml](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/alpha_context.ml) :
        - introducing everything which can be stored in context as modules
        - the real structure type underneath is defined in [raw_context](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/raw_context.ml) as `t`, in other words, you can find the concrete definition of context in [here](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/raw_context.ml#L28)

A **context** is a block state and sometimes also known as a ledger’s state

# Protocol Overview

From *How to start reading protocol Alpha* on tezos.gitlab.io

The Protocol is defined in a layered hierarchy. On the one end of this hierarchy is a node-runner[1] who executes the protocol as a program. On the other end, it’s the low level raw key-value database which is used for storing block context. You can fine this hierarchy in file [TEZOS_PROTOCOL](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/TEZOS_PROTOCOL).

Roughly speaking, the hierarchy can be described as following layers

- `Main`
- `*_services`
- `Baking`, `Amendment`, `Apply_results`, `Apply`
- `Script_*`
- `Alpha_context`
- `*_storage`
- `Raw_context`, `Storage_*`
- `*_repr`, `Michelson_v1_primitives`, `Lazy_storage_kind`
- `Storage_description`, `*_hash`

TODO ←  adding explanation for each of those *layer*

Here we use “*protocol layer”* to indicate the protocol part of entire Tezos. And, for avoiding confusion, we use “*module layer”*, or just “*layer”*, to indicate a group of modules which defines or provides a certain functionality for/in the *protocol layer.*

From the abstraction perspective, the hierarchy is separated by the module `Alpha_context` which defines the block context. It’s an abstraction of “how a context will be stored in a data storage (a key-value database)” and is used by higher layers of abstraction. For example, you can find it be opened in module like `Script_interpreter` or `Apply`, but not in `Contract_storage` or `Storage`.

The module `Alpha_context` defines the block context. That is to say, it defines all data structures for the protocol layer. This is important because, if we think the protocol layer as an interpreter which takes an old context,  and returns a new context.

By its nature, I would take it as a

- ``*_Storage`
- Storage
    - 定義 Index
        - 使用 index + ? 定義出各種被稱為 storage 的 container
    - 定義出 protocol 上的 context 結構
        - 以及相配套的 類-context 結構
- Representation
    - data for protocol layer
    - i.e. primitive “value”
- JSON + Binary
    - underneath raw data

## Storage

storage 最底層有兩個檔案 **storage_sigs** 和 **raw_context**，其中

- storage_sigs
    - 描述了所有不同功能與用途的 storage 的 spec.
    - 描述了 NAME 和 VALUE 的 spec
    - 注意 Indexed_raw_context 是一種叫作 context 的 storage
- raw_context
    - 提供了 context 實際的定義

往上一層是 **storage_functors**，其中

- open storage_sigs
- 大量使用 Raw_context 這個 module 提供的函數/modules
- 描述了 INDEX 的 spec
- 提供了 Make function - 在給定 context, index, value 時, 如何產生 storage 的實際 module 定義

再往上一層是 **storage**，其中

- open storage_functors
- 提供所有主要的資料結構
    - 其中包含一些 refined 或是 redefined 而來的 context

```ocaml
open Storage_sigs

module Block_priority : sig
module Roll : sig
module Contract : sig
module Big_map : sig
module Sapling : sig
module Vote : sig
module Seed : sig
module Commitments :
  Indexed_data_storage
    with type key = Blinded_public_key_hash.t
     and type value = Tez_repr.t
     and type t := Raw_context.t

module Ramp_up : sig
```

`*_repr` 描述 protocol layer 的基本資料型別。並且描述該型別如何被以 data_encoding 來轉換成 serialization format (亦即 binary form 和 JSON)

- Data_encoding 是Nomadic Lab 在 maintain 的 ocaml lib.
- `*_repr` 普遍會 `open Data_encoding`
- Tezos 使用 Data_encoding 作為 external lib
    - 從 lib_protocol_environment 中可以找到其如何被引入.
- 提供了將任意 data structure 給 encode/decode 成 json 或 binary representation.
- https://gitlab.com/nomadic-labs/data-encoding

The module **Alpha_context** represents the abstraction of block state

there is a middle layer of this hierarchy


Main - Apply - low level storage

- [1] could be a baker, validator, etc..

Each layer is an abstraction

In,
The main modules can be grouped by .
