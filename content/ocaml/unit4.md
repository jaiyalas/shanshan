---
title: "⟦4⟧️ Polymorphism"
---

# Polymorphism,

- polymorphic, (adj.) 多型的, 多態的
  - 通常是指涉 polymorphic function
  - 指可以處理很多不同 input 種類的 function
- Polymorphism
    - 對任意一種 input 都做同樣的事情 => parametric polymorphism
    - 對給定的某種 input 作特定的某種事情 => ad-hoc polymorphism
        - 窮舉定義; 沒舉就不知道
    - 僅對某些特定的 input 類型做事情 => inclusion polymorphism (subtype)
        - 具有一定程度的推敲能力
        - 沒舉也可能會知道

## Parametric polymorphism

- universal quantifier: $$\forall a . a \rightarrow a$$
- 通常是 contrainer 相關的 data structure 最常見
    - $$\forall a\ b\ .\ (a \to b) \to List\ a \to List\ b$$
    - `map :: (a -> b) -> [a] -> [b]`

### free theorem

`f :: $$\forall a\ .\ a \to a$$`

> Q: what’s the possible definition of above `f` ?

  R:  $$f \equiv \lambda x.x$$ (i.e., id function)

- why?

`g :: $$\forall a\ b\ .\ a \to b$$`

> Q: how about `g`?


## Ad-hoc polymorphism

- function/operator overloading
```ocaml
add :: Int -> Int -> Int
add i j = Int.add i j

add :: Double -> Double -> Double
add m n = Double.plus m n
```

- adding different type constrain; and then,

```ocaml
defining corresponding definition
add :: Addable num . num -> num -> num

def Int is Addable
  add(i1, i2) = Int.add(i1,i2)
def Double is Addable
  add(d1, d2) = Double.add(d1, d2)
```

## Inclusion polymorphism

- also-known as subtype, subtyping, etc..

    [Definition]
    given type $$t_1$$ and $$t_2$$, subtyping relation: $$t_1 \preceq t_2$$ holds
    if all of the information in $$t_1$$ is also existing in  $$t_2$$.

- The problem is, what is “all of the information is also existing”?
    - OO: $$t_1$$ inherit $$t_2$$
    - ADT: all of data constructors (and their associated types) of $$t_1$$ is strictly less or equals to $$t_2$$

## Notes

|            | Haskell                     | OCaml              |
| ---------- | --------------------------- | ------------------ |
| Ad-hoc     | YES                         | 🤔 (OO overriding) |
| Parametric | YES                         | YES                |
| Inclusion  | 🤔 (type-level programming) | YES                |


# Types in OCaml

- type-definition: introduce ocaml keyword - `type` `…`
![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600822505222_image.png)

- typedef: actual type definition
![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600822627971_image.png)
    - typeconstr-name $$\equiv$$ lowercase-ident
    - `type` `<some_params> typename <its_definition>`

- typedef
![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600822627971_image.png)

- type-params: it’s one type-param; or, a tuple of at least one type-param
![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600822745048_image.png)

- typedef
![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600822627971_image.png)

- type-param: ($$\forall$$ quantified) type variable
![variance ::= + | -](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600823854107_image.png)

- typedef
![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600822627971_image.png)

- type-information: definition body of type
![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600823149850_image.png)

- type-information
![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600823149850_image.png)

![the LHS ident must be an (OO) instance of the RHS typexpr](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600823743877_image.png)

- type-information
![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600823149850_image.png)

![constructing via ADT or class](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600823190726_image.png)
![constructing via constructor or record](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600823208138_image.png)

![](https://paper-attachments.dropbox.com/s_E950158A9F6FCC2C986FF24EE0D60223599F6F8466006D0DA1B7ADF8B758BBD6_1600824553377_image.png)

- a typexpr is:
    - ADT
        - closed(?)
            - type val (i.e. another type)
            - type var
            - type tupling
            - function type (without tvar-intro.)
            - type function application
            - renaming
        - polymorphic variant
    - class related declaration

## Type variable introduction

- no matter how we construct a type,
    new tvar can always be introduced in terms of type params
    type <type_params> typename = <type_def> ;;
- wait, that’s it? nowhere else?

### Case 1: Nope!

- `type` `<params> typename` `=` `<def_by_ADT_closed> ;;`
    - closed and cannot intro. any tvar.
    - tvar can NOT be introduced in definition
- `type` `<params> typename` `=` `<def_by_constr> ;;`
    - e.g.  `| CName` `of` `<tuple_of_types>`
    - tvar can NOT be introduced in definition

### Case 2: The poly-typexpr

```ocaml
    poly-typexpr ::= typexpr | { ' ident }+ .  typexpr

    field-decl   ::= [mutable] field-name : poly-typexpr
    method-type  ::= method-name : poly-typexpr
```

- record type
    - `type` `<params> typename` `=` ```{` `f1 : 'a . 'a ; ..` `}` `;;`
    - tvar can be introduced in terms of field
- class method
    - `type` `<params> typename` `=` ```<` `m1 : 'a . 'a ; ..` `>`  `;;`
    - tvar can be introduced in terms of method

### Case 3: The polymorphic variant types

- A polymorphic variant type is recursively defined by Tags and typexpr only. [link]
- yet, typexpr contains the case of method type where a new tvar can be introduced
- it’s no tvar intro. if there is no method type involved

## Location of forall quantifier

```ocaml
type 'a t1 = { f1 :      'a -> int }
type    t2 = { f2 : 'a . 'a -> int }

let d1a = { f1 = function 0 -> 0 | _ -> 1 }
and d1b = { f1 = function false -> 0 | _ -> 1 }
in d1a, d1b ;; (* int t1 * bool t1 *)

let d2a = { f2 = function 0 -> 0 | _ -> 1 } ;;
(* 💀 This field value has type int -> int
   which is less general than 'a. 'a -> int *)

let d2a = { f2 = function _ -> 1 } ;;
```

# Polymorphism in OCaml

- Parametric polymorphism, can be realized in terms of
    - type params
    - field declaration of record type
    - method declaration of class
- Subtyping
    - please read document of the polymorphic variant type
- Ad-hoc polymorphism
    - method (type) overriding
    - please read document of the object system in OCaml
