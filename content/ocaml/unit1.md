---
title: "⟦1⟧️ La Base"
---

# Types

OCaml 中基本的資料型別有三類：

- Primitive types
- Algebraic data types
- Record type

## Primitive type

(type primitif)

Primitive types 就都是些常見的型別：

這邊列出整數 (int) 和文字 (char/string) 的規範：

```
integer-literal ::= [-] (0…9) { 0…9 ∣  _ }
  ∣  [-] (0x∣ 0X) (0…9∣ A…F∣ a…f) { 0…9∣ A…F∣ a…f∣ _ }
  ∣  [-] (0o∣ 0O) (0…7) { 0…7∣ _ }
  ∣  [-] (0b∣ 0B) (0…1) { 0…1∣ _ }

escape-sequence ::= \ ( \ ∣  " ∣  ' ∣  n ∣  t ∣  b ∣  r ∣  space )
  ∣  \ (0…9) (0…9) (0…9)
  ∣  \x (0…9∣ A…F∣ a…f) (0…9∣ A…F∣ a…f)
  ∣  \o (0…3) (0…7) (0…7)
```

數字細分為整數和浮點數，兩者自表達哪至於使用的計算符號，
都是相當於是使用兩個不同的 namespaces。
整數使用一般常見的 `+ - * /`；
但是浮點數使用的則是 `+. -. *. /.`。
兩種不同類型的數字之間是不會自動轉換的。

字串也分為一般常見的 string `"..."` 以及 quoted string `{|...|}` 兩種。
此外亦有提供內建的 concat: `(_^_) : string -> string -> string` 。

Pair 和 List 也都是蠻標準的語法： `( 1 , 2 )` 和 ` 1 :: 2 :: []`。
但是要注意的是，兩者都有比較特殊的簡易表示法： `1 , 2` 以及 `[1 ; 2]`。
注意， list 的 `[` 以及 `]` 是不能省略的。

## Algebraic data type

(Type algébrique de données)

### Product and Coproduct

- type/data constructor of product type:
    <type> * <type>
    <data> , <data>


- co-product type  又稱 sum type ，在 ocaml 中被稱為 variant (type)
- type constructor of co-product type:
    <type> | <type>

### type declaration

- type constructors 用  `'_`  來 declare
    type variable (as parameter).
- data constructors 用  `_ of _`  來 parametrize.
![](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600409876095_image.png)

```ocaml
type    t0 = |
and  'a t1 = C1 of 'a | C2 of int | C3
and     t2 = | A
and     t3 = t2
and     t4 = t2 = A ;; (* 😵  *)

type t_nope1 = A | ;;      (* 💀 : end with | *)
type t_nope2 = t2 = t3 ;;  (* 💀 : two type-equations *)
type t_nope3 = t2 | B ;;   (* 💀 : conflit constructor *)
```

## Record type

(Enregistrement (structure de données))

![](https://paper-attachments.dropbox.com/s_F7E14C29080210A343C8E3AE8BF4EC5E3E7DFFC256BF505BBEBC630CFE41A3D0_1600572470792_image.png)
![](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600412301356_image.png)

record type, 一如往常 (as in Haskell)

```ocaml
type rt1 = { f1 : t1 ; f2 : t2 ; }
and  rt2 = { foo : 'a . 'a -> int } (* 么壽唷 *)
and  rt3 = { mutable foo : 'a . 'a -> int } (* ㄟ(=口=)ㄟ *)
```

### Initialization

![](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600414812157_image.png)

- ocaml 的 record 是沒有 constructor 也沒有預設順序的，
    所以要用 totally pass-by-name 的方式去填滿 fields ：
- ocaml 也提供額外的 build upon another record 的語法, 但是至少其中一個 field 要賦值
    注意： `{ x with … }` will make a copy!

```ocaml
let x = { f1=1 ; f2=2 }
let y = {x with f1 = 3}
let z0a = {y with f1}      (* 💀 : 看起來 = expr 是必要的 *)
let z0b = {y with f1; f2}  (* 💀 : 看起來 = expr 是必要的*)
let z1 = {y with f2 = y.f2} (* 蠢-clone *)

type rt1 = { f : 'a 'b . 'a -> 'b -> 'b } ;;
let foo _ y = y in
let goo _ = fun x -> x in
let x = {f = foo} in
let y = {x with f = goo} in
x.f 1 false , y.f false 1 ;; (* 😵 => (false,1) *)
```

### Accessing

- haskell 可以用 projections 或是 pattern matching 去 destruct record
- OCaml 沒有預設的 proj. 可以用, 但是有兩個 operators:

```ocaml
(* eval with <expr1>.<field1> *)
let left = x.f1 in

(* update with <expr1>.<field1> <- <expr2> *)
x.f2 <- (something : t2)
```

- Pattern matching 幾乎是一樣，都是用 pattern: `{<field name>=<new val>; …}` 。

注意：這邊也可以使用 wildcard: `_` 來取代所有 unmentioned fields；

```ocaml
type rt1 = { f1 : int ; f2 : int } ;;
(* rename as var. *)
let foo {f1=x; f2=y} = x + y in
let foo' x = match x with {f1=x; f2=y} -> x + y in
(* forcing type checking *)
let goo {f1:int=f1;f2=f2} = f1 + f2 in
(* rename as same *)
let goo {f1=f1;f2=f2} = f1 + f2 in
(* sugar *)
let goo' {f1;f2} = f1 + f2 in

(* wildcard *)
type rt2 = {fa:int; fb:int; fc:int} ;;
let hoo {fa=1;_} = x in
```

- 注意：lexical scoping 會使得 record 看起來更有機會看不懂, pattern (LoC:1) 和 term (LoC4,6) 視覺上會沒有區別

```ocaml
let foo {f1;f2} = ... in

let f1 = 1 and f2 = 2 in
let x = {f1; f2} in

let foo f1 f2 = {f1;f2} in
```

## Ambiguously inferring and matching

- 避免 matching 或是 type inference 時出現 ambiguous cases. 最好的辦法是提供 type information explicitly
- 給定 env $$\Gamma$$ 和 constructor $$C_0$$ , type inference process 會試著找出 $$\tau$$,  $$\Gamma \vdash C_0 : \tau$$ . Ocaml 在這過程中會優先從去 match  $$\Gamma$$ 中第一個 matchable 的結果； 也就是說，如果沒有 give type information explicitly， 那 ocaml 會去使用距離最近 (last defined) 的那一個 type。
- 注意：在 pattern matching 時，這種預設選擇順序是 greedy 的。 ocaml 不會因為後面出現 refutable case 而重新尋找更合適的定義：

```ocaml
type first = A | B ;;
type second = A ;;

let broken x =
  match x with
  | A -> 0 (* 這邊會過 *)
  | B -> 1 (* 這裡會該 B 的 type 不是 second *)

(* 但是如果 first 和 second 交換順序就沒問題了! *)
```

# Stateful Computation

- Array
- Reference
- Dirty computation

## Array (mutable linear structure)

- length limitation = 254 − 1
- 和 record 有相似的 operators

```ocaml
(* array 宣告 *)
[| v1 ; v2 ; ...|]
(* array indexed getter *)
arr.(i)
(* array indexed setter *)
arr.(i) <- val
```

- 可以直接用 array 語法來作 array pattern

## Reference

- OCaml 有提供 mutable reference 可以用

```ocaml
(* reference *)
let addr = ref x in
(* fetching *)
let x' = !addr in
(* assignment *)
addr := y
```

- reference is totally definable in OCaml:

```ocaml
    type 'a ref = { mutable contents: 'a };;
    let ( ! ) r = r.contents;;
    let ( := ) r newval = r.contents <- newval;;
```

## ~~dirty~~ stateful computation

- reference can be used for side-effect-wise computation

![](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600361751199_image.png)


- with mutable field, can be used for making ad-hoc polymorphism

![](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600361923356_image.png)

- why not using monad? it provides more controllability on side-effect

# Evaluation Control

- lazy
- except

## Lazy

- 應該是某一種 call-by-need；任何 lazy expr 算過一次就會被 bind 到 result value 上
- be-lazy

```ocaml
let lazy_x = lazy (<expr>) in
```

- 注意，這裡的 `let lazy x2` 的 `x2` 其實也是一種 matchable。這表示 lazy pattern 裡面的 lazy 其實是 de-lazy 的意思。這使得 `| lazy _ -> ...`  這種情況中，就算 pattern 是 wildcard ，也還是會被強制先計算
- de-lazy

```ocaml
(* method 1 *)
let x1 = Lazy.force lazy_x

(* method 2 *)
let lazy x2 = lazy_x
let fo x = match x with lazy 1 -> 1 | _ -> 0
```

## Exception

- 推測 exception 是一種 datatype à la carte 的 suger
- 具有兩種語法: introduce 和 renaming

```ocaml
exception MyExcept1 ;;
exception MyExcept2 = M1.ExistingExcept ;;
```

```ocaml
(* val raise : exn -> 'a [Stdlib] *)
let f x y = if x > y then raise MyExcept1 else 0 in

(* traditional catch *)
try expr with | E1 -> expr1 ..

(* sugar with match-with *)
match something with
| exception E1 -> .. (* catch *)
| C1 | exception E2 -> .. (* catch with precondition *)
| x -> .. (* no exception*)
```

- 如果要用來作 control-flow 的手段，建議是創造一些 local exception 來作為 flow 中斷的手段。

# Label

- Label, a named parameter
- 只會出現在 argument 或 parameter 上

![parameter](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600535212151_image.png)

![argument](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600535230219_image.png)

```ocaml
(* pattern match *)
let x = (3, 7)
and foo ~xin:(x1,x2) = x1 + x2 in
foo ~xin:x ;;

(* typing *)
let goo ~(x:int) = x + 1 in
goo ~x:10 ;;
```

## optional label

![parameter](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600536209120_image.png)

![argument](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600536191631_image.png)

- ⚠️  `?`  也是 label，所以要用  `~`  去 pass
- ⚠️ 對多參數函數,  `?x:None`  可以用來 disable 名為 x 的 parameter.

## 跛腳 type inference

- ⚠️ parameter types are well-ordered => `~(x:int) ~(y:int) ≠ ~(y:int) ~(x:int)`
- ⚠️ `~x` 和 `?x` 會不相容:

```ocaml
    let h ?i = i+1 in       (*   h : ?i:int -> int = <fun> *)
    let foo f = f ~i:1 in   (* foo : (~i:int -> int) -> int = <fun> *)

    foo h => (* 💀 : ?i:int ≠ ~i:int  *)
```

## Notes

- makes programs more readable,
- is easy to remember,
- when possible, allows useful partial applications.

![](https://paper-attachments.dropbox.com/s_E967BDF9EB5DE34395BE25025E420367A90337AE32C5A69A09A7BAB121A7C031_1600536927887_image.png)
