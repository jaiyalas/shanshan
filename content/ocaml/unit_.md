---
title: "🏷 extra"
---

# unknown

## example 1

```ocaml
type rt = {mutable f : int} ;;
let x1 = {f = 1} in
let x2 = ((x1.f <- 2) ; x1.f) in
x2 ;;
```

- sequence and double semicolon
