---
title: "quick notes"
date: 2021-02-09
hidden: false
---


# tl;dr


- **securities** (證券/有價證券) ，諸如 stock (股票), bond (債券), warrant (權證)；是一種表示財產權的有價憑證，持有者可以依據此憑證，證明其所有權或債權等私權的證明文件。
- **liquidity**, as a _property of security_, means the capability of being converted into cash. 證券無法或是難以交易/變現被稱為停止流動。
- A **liquidity pool** is a collection of funds locked in a smart contract.
  - DEX = Decentralized EXchanges.
  - users are called **liquidity providers**.
- liquidity providers 將等價的 A, B 兩種 token 放入池中來 __create a market__. as result, liquidity providers 將獲得 trading fee (根據所佔之百分比重)。
- trade → order => order book
- matching machine (CEX) match two orders in the order book
- _credit_ A to B (v) 匯款 A 金額給 B 帳號/人

# intuition

# background

# definition
