---
title: "quick commonds"
date: 2020-01-01
hidden: false
---

## search on gitlab

- `text` + `extension:subname<>`
- `text` + `pth:<folders>`
- `text` + `filename:<name>`
- extra prefix modifier: `-` for exclusion

## nix

```bash
> nix-nev -qaP ...
> nix-nev -i ...
> nix-nev -e ...
```

## connect to EC2 via pem

```bash
> ssh -i <path/to/.pem> <user name>@<public ip>
```

e.g.

```bash
> ssh -i ~/Downloads/aws_eatt1029_inst01.pem ubuntu@18.191.42.62
```
