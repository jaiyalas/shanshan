---
title: "string"
date: 2020-10-10
hidden: false
---

Haskell 三大文字種類: String, ByteString, Text；

三者之間被廣泛使用於不同的 libraries 中，
如何轉換和選擇就是個學問了。
